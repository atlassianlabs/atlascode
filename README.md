# This repository has been moved to GitHub.

Please see https://github.com/atlassian/atlascode

## Issues

[GitHub](https://github.com/atlassian/atlascode/issues) will track new issues.

[Bitbucket](https://bitbucket.org/atlassianlabs/atlascode/issues) will track old issues; so we don't lose votes.

## Pull Requests

Please use [GitHub for Pull Requests](https://github.com/atlassian/atlascode/pulls). This keeps GitHub as the source of truth. 